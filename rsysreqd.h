#ifndef __RSYSREQD_H
#define __RSYSREQD_H

#include <netdb.h>
#include <stdbool.h>
#include <stdlib.h>

extern const char* RSYSREQD_PORT;
extern const char* RSYSREQD_NAME;

typedef struct {
	int num;
	char *error;
} rsysreqd_error;

typedef struct {
	struct sockaddr_storage *saddr;
	char *msg;
	size_t msg_len;
	socklen_t slen;
	int sock;
} rsysreqd_conn;

typedef struct {
	char *addr;
	char *port;
	bool verbose;
} rsysreqd_opts;

rsysreqd_error *rsysreqd_error_new(void);
void rsysreqd_error_free(rsysreqd_error *err);

rsysreqd_conn *rsysreqd_conn_new(void);
void rsysreqd_conn_free(rsysreqd_conn *conn);

rsysreqd_opts *rsysreqd_opts_new(void);
void rsysreqd_opts_free(rsysreqd_opts *opts);

int rsysreqd_getsocket(rsysreqd_opts *opts, rsysreqd_error **err);
bool rsysreqd_procmsg(rsysreqd_conn *conn, rsysreqd_error **err);
bool rsysreqd_getpkts(int sock, rsysreqd_error **err);

void rsysreqd_init_logging(rsysreqd_opts *opts);
void rsysreqd_close_logging(void);
void rsysreqd_log_error(const char* message, ...);

#endif

