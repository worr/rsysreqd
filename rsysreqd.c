#include "rsysreqd.h"

#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

const char* RSYSREQD_NAME = "rsysreqd";
const char* RSYSREQD_PORT = "7";

rsysreqd_error *rsysreqd_error_new(void) {
	rsysreqd_error *err = NULL;
	if ((err = malloc(sizeof(rsysreqd_error))) == NULL) {
		rsysreqd_log_error("rsysreqd_error_new");
		return NULL;
	}

	memset(err, 0, sizeof(rsysreqd_error));
	return err;
}

void rsysreqd_error_free(rsysreqd_error *err) {
	free(err->error);
	free(err);
}

rsysreqd_conn *rsysreqd_conn_new(void) {
	rsysreqd_conn *conn = NULL;
	if ((conn = malloc(sizeof(rsysreqd_conn))) == NULL) {
		rsysreqd_log_error("rsysreqd_conn_new");
		return NULL;
	}

	memset(conn, 0, sizeof(rsysreqd_conn));
	return conn;
}

void rsysreqd_conn_free(rsysreqd_conn *conn) {
	free(conn->msg);
	free(conn->saddr);
	free(conn);
}

rsysreqd_opts *rsysreqd_opts_new(void) {
	rsysreqd_opts *opts = NULL;
	if ((opts = malloc(sizeof(rsysreqd_opts))) == NULL) {
		rsysreqd_log_error("rsysreqd_opts_new");
		return NULL;
	}

	memset(opts, 0, sizeof(rsysreqd_opts));
	opts->port = strdup(RSYSREQD_PORT);
	return opts;
}

void rsysreqd_opts_free(rsysreqd_opts *opts) {
	if (opts->addr) free(opts->addr);
	if (opts->port) free(opts->port);
	free(opts);
}

int rsysreqd_getsocket(rsysreqd_opts *opts, rsysreqd_error **err) {
	assert(!*err);

	struct addrinfo hints, * serverinfo, * p = NULL;
	int ret = -1;
	int sock = 0;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;

	if ((ret = getaddrinfo(opts->addr, opts->port, &hints, &serverinfo)) != 0) {
		*err = rsysreqd_error_new();
		(*err)->num = ret;
		(*err)->error = strdup(gai_strerror(ret));
		return -1;
	}

	for (p = serverinfo; p != NULL; p = p->ai_next) {
		if ((sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
			continue;

		if (bind(sock, p->ai_addr, p->ai_addrlen) == -1) {
			close(sock);
			continue;
		}

		break;
	}

	if (p == NULL) {
		close(sock);
		freeaddrinfo(serverinfo);

		*err = rsysreqd_error_new();
		(*err)->num = -1;
		(void)asprintf(&(*err)->error, "Cannot listen on port 7: %s", strerror(errno));
		return -1;
	}

	freeaddrinfo(serverinfo);

	return sock;
}

/* conn must be free'd */
bool rsysreqd_procmsg(rsysreqd_conn *conn, rsysreqd_error **err) {
	assert(!*err);

	FILE *f = NULL;

	for (size_t i = 0; i < conn->msg_len - 1; i++) {
		if ((f = fopen("/proc/sysrq-trigger", "w")) == NULL)
			goto rsysreqd_procmsg_bad;

		syslog(LOG_INFO, "Writing %c to sysrq-trigger", conn->msg[i]);
		fprintf(f, "%c\n", conn->msg[i]);

		if (fclose(f))
			goto rsysreqd_procmsg_bad;
	}

	return true;

rsysreqd_procmsg_bad:

	*err = rsysreqd_error_new();
	(*err)->error = strdup(strerror(errno));
	(*err)->num = errno;
	return false;
}

bool rsysreqd_resp(rsysreqd_conn *conn, rsysreqd_error **err) {
	assert(!*err);

	if (sendto(conn->sock, conn->msg, conn->msg_len, 0,
			(struct sockaddr *)conn->saddr, conn->slen) < 0) {

		*err = rsysreqd_error_new();
		(*err)->error = strdup(strerror(errno));
		(*err)->num = errno;
		return false;
	}

	return true;
}

static char* sockaddr_to_ip(struct sockaddr_storage *saddr, socklen_t slen) {
	char *ip = NULL;
	if (saddr->ss_family == AF_INET) {
		if ((ip = malloc(INET_ADDRSTRLEN)) == NULL) {
			rsysreqd_log_error("%s: %s", "malloc", strerror(errno));
			goto sockaddr_to_ip_bad;
		}
		memset(ip, 0, INET_ADDRSTRLEN);

		if (!inet_ntop(saddr->ss_family, &((struct sockaddr_in*)saddr)->sin_addr, ip, slen)) {
			rsysreqd_log_error("%s: %s", "inet_ntop", strerror(errno));
			goto sockaddr_to_ip_bad;
		}
	} else if (saddr->ss_family == AF_INET6) {
		if ((ip = malloc(INET6_ADDRSTRLEN)) == NULL) {
			rsysreqd_log_error("%s: %s", "malloc", strerror(errno));
			goto sockaddr_to_ip_bad;
		}
		memset(ip, 0, INET6_ADDRSTRLEN);

		if (!inet_ntop(saddr->ss_family, &((struct sockaddr_in6*)saddr)->sin6_addr, ip, slen)) {
			rsysreqd_log_error("%s: %s", "inet_ntop", strerror(errno));
			goto sockaddr_to_ip_bad;
		}
	}

	return ip;

sockaddr_to_ip_bad:
	if (ip) free(ip);
	return NULL;
}

bool rsysreqd_getpkts(int sock, rsysreqd_error **err) {
	assert(!*err);

	char *ip = NULL;
	char buf[BUFSIZ];
	memset(buf, 0, BUFSIZ);
	ssize_t recv_len = 0;
	struct sockaddr_storage saddr;
	socklen_t slen = sizeof(struct sockaddr);
	rsysreqd_conn *conn = NULL;

	struct sigaction sa, prevsa;
	sa.sa_handler = SIG_DFL;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART | SA_NOCLDWAIT | SA_NOCLDSTOP;

	if (sigaction(SIGCHLD, &sa, &prevsa) == -1) { 
		rsysreqd_log_error("sigaction: %s", strerror(errno));
		return false;
	}

	while (true) {
		if ((recv_len = recvfrom(sock, buf, sizeof(buf), 0,
				(struct sockaddr *)&saddr, &slen)) < 0) {

			*err = rsysreqd_error_new();
			(*err)->error = strdup(strerror(errno));
			(*err)->num = errno;
			return false;
		}

		switch (fork()) {
			/* CHILD */
			case 0:
				conn = rsysreqd_conn_new();
				if ((conn->saddr = malloc(sizeof(struct sockaddr_storage))) == NULL) {
					rsysreqd_log_error("malloc: %s", strerror(errno));
					rsysreqd_conn_free(conn);
					exit(3);
				}

				conn->msg = strndup(buf, recv_len);
				conn->msg_len = recv_len;
				conn->sock = sock;
				memcpy(conn->saddr, &saddr, sizeof(struct sockaddr_storage));
				conn->slen = slen;

				if (!rsysreqd_procmsg(conn, err)) {
					rsysreqd_log_error("%s: %s", "rsysreqd_procmsg", (*err)->error);
					rsysreqd_error_free(*err);
					rsysreqd_conn_free(conn);
					exit(1);
				}

				if (!rsysreqd_resp(conn, err)) {
					rsysreqd_log_error("%s: %s", "rsysreqd_resp", (*err)->error);
					rsysreqd_error_free(*err);
					rsysreqd_conn_free(conn);
					exit(2);
				}

				rsysreqd_conn_free(conn);
				exit(0);
			case -1:
				*err = rsysreqd_error_new();
				(*err)->error = strdup(strerror(errno));
				(*err)->num = errno;
				return false;
			/* PARENT */
			default:
				ip = sockaddr_to_ip(&saddr, slen);
				if (ip) {
					syslog(LOG_INFO, "Incoming connection from %s", ip);
					free(ip);
				}

				ip = NULL;
				break;
		}
	}

	return true;
}

void rsysreqd_init_logging(rsysreqd_opts *opts) {
	openlog(RSYSREQD_NAME, LOG_PID|LOG_PERROR, LOG_DAEMON);

	if (!opts->verbose)
		(void)setlogmask(LOG_UPTO(LOG_ERR));

	syslog(LOG_INFO, "Starting %s", RSYSREQD_NAME);
}

void rsysreqd_close_logging(void) {
	syslog(LOG_INFO, "%s", "Shutting down");
	closelog();
}

void rsysreqd_log_error(const char* message, ...) {
	va_list args;

	va_start(args, message);
	vsyslog(LOG_ERR, message, args);
	va_end(args);
}

