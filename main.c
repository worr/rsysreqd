#include "rsysreqd.h"

#include <string.h>
#include <stdio.h>
#include <unistd.h>

static void usage(const char *basename) {
	fprintf(stderr, "Usage: %s [-h] [-i address] [-p port] [-v]\n", basename);
	fprintf(stderr, "\t-h\tPrint this usage text\n");
	fprintf(stderr, "\t-i\tAddress to listen on\n");
	fprintf(stderr, "\t-p\tPort to listen on\n");
	fprintf(stderr, "\t-v\tVerbose mode\n");
	exit(1);
}

int main(int argc, char **argv) {
	int sock =  -1;
	rsysreqd_error *err = NULL;
	int opt;

	rsysreqd_opts *opts = rsysreqd_opts_new();
	while ((opt = getopt(argc, argv, "i:p:v")) != -1) {
		switch (opt) {
			case 'i':
				if (opts->addr) free(opts->addr);
				opts->addr = strdup(optarg);
				break;
			case 'p':
				if (opts->port) free(opts->port);
				opts->port = strdup(optarg);
				break;
			case 'v':
				opts->verbose = true;
				break;
			case '?':
				if (optopt == 'i' || optopt == 'p') {
					fprintf(stderr, "-%c requires an argument\n", optopt);
					usage(argv[0]);
				} else if (optopt == 'h') {
					usage(argv[0]);
				} else {
					fprintf(stderr, "Unknown option %c\n", optopt);
					usage(argv[0]);
				}
			default:
				usage(argv[0]);
		}
	}

	rsysreqd_init_logging(opts);

	if ((sock = rsysreqd_getsocket(opts, &err)) == -1) {
		rsysreqd_log_error("%s: %s", "rsysreqd_getsocket", err->error);
		rsysreqd_error_free(err);
		rsysreqd_close_logging();
		return EXIT_FAILURE;
	}

	// opts don't matter anymore
	rsysreqd_opts_free(opts);

	if (!rsysreqd_getpkts(sock, &err)) {
		rsysreqd_log_error("%s: %s", "rsysreqd_getpkts", err->error);
		rsysreqd_error_free(err);
		rsysreqd_close_logging();
		return EXIT_FAILURE;
	}

	rsysreqd_close_logging();
	return EXIT_SUCCESS;
}

