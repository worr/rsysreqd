CC		?= cc
LD		?= cc
CFLAGS	?= -O3
LDFLAGS	?=
DESTDIR	?= /usr/local
INSTALL	?= install
GZIP	?= gzip

DEFAULT_CFLAGS	:= -Wall -Werror -Wextra -Wno-unused-result -pedantic -std=c99 -D_GNU_SOURCE -fomit-frame-pointer
DEFAULT_LDFLAGS	:= -lc
OBJS			:= rsysreqd.o main.o
CFLAGS			:= $(CFLAGS) $(DEFAULT_CFLAGS)
LDFLAGS			:= $(LDFLAGS) $(DEFAULT_LDFLAGS)

.PHONY : clean install deinstall

rsysreqd: $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@ 

%.o : %.c
	$(CC) -c $(CFLAGS) $<

clean:
	-rm $(OBJS) rsysreqd

install: rsysreqd
	@[ -d $(DESTDIR)/sbin ] || mkdir -p $(DESTDIR)/sbin
	$(INSTALL) -o 0 -g 0 -m 0755 $< $(DESTDIR)/sbin/$<
	@[ -d $(DESTDIR)/share/man/man1 ] || mkdir -p $(DESTDIR)/share/man/man1
	$(INSTALL) -o 0 -g 0 -m 0644 rsysreqd.1 $(DESTDIR)/share/man/man1/rsysreqd.1
	$(GZIP) -9 -f $(DESTDIR)/share/man/man1/rsysreqd.1

deinstall:
	-rm -f $(DESTDIR)/sbin/rsysreqd
	-rm -f $(DESTDIR)/share/man/man1/rsysreqd.1.gz
	@-rm -f $(DESTDIR)/share/man/man1/rsysreqd.1 2> /dev/null || :
	@-rmdir $(DESTDIR)/sbin 2> /dev/null || :
	@-rmdir $(DESTDIR)/share/man/man1 2> /dev/null || :
	@-rmdir $(DESTDIR)/share/man 2> /dev/null || :
	@-rmdir $(DESTDIR)/share 2> /dev/null || :
